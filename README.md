# Discord Linux Issue Tracker

The place for helpful advice and bug reports.

## Usage

If you happen to find a bug within the discord-linux system or want something changed or added to an image, feel free to open an issue at this git for assistance and bug fixes.

Please keep in mind, this project is a single sole developer, replies and bug fixes may be slow.

